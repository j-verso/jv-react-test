/* global fetch:false */
import { fetchFavouritesActionCreator, addFavouritesActionCreator, delFavouritesActionCreator, TOGGLE_FAVOURITE_TYPE, REHYDRATED } from '../actions'
import { getFavouritesApiUrl, isFavouritedSelector } from '../selectors'

const fetchFavourites = async (apiUrl) => {
  let url = apiUrl
  const response = await fetch(url)
  const favourites = await response.json()

  if (!response.ok || !favourites) {
    const error = new Error('Failed to fetch favourites')
    error.status = response.status
    throw error
  }
  return favourites
}

const addFavourite = async (apiUrl, theId) => {
  let url = apiUrl + '/' + theId
  const response = await fetch(url, {
    method: 'PUT',
    body: JSON.stringify({ id: theId })
  })

  const newFavourites = await response.json()

  if (!response.ok || !newFavourites) {
    const error = new Error('Failed to add favourite')
    error.status = response.status
    throw error
  }
  return newFavourites
}

const delFavourite = async (apiUrl, theId) => {
  let url = apiUrl + '/' + theId
  const response = await fetch(url, {
    method: 'DELETE',
    body: JSON.stringify({ id: theId })
  })
  const newFavourites = await response.json()
  if (!response.ok || !newFavourites) {
    const error = new Error('Failed to del favourite')
    error.status = response.status
    throw error
  }
  return newFavourites
}

export default store => next => action => {
  const ret = next(action)

  if (action.type === REHYDRATED) {
    const state = store.getState()
    const apiUrl = getFavouritesApiUrl(state)
    store.dispatch(fetchFavouritesActionCreator(fetchFavourites(apiUrl)))
  }

  if (action.type === TOGGLE_FAVOURITE_TYPE) {
    const state = store.getState()
    const apiUrl = getFavouritesApiUrl(state)
    const eventId = action.payload.entityId

    const isAlreadyFavourited = isFavouritedSelector(state, eventId)

    if (isAlreadyFavourited) {
      store.dispatch(delFavouritesActionCreator(delFavourite(apiUrl, eventId)))
    } else {
      store.dispatch(addFavouritesActionCreator(addFavourite(apiUrl, eventId)))
    }
  }

  return ret
}
