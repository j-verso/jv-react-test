export const getFavourites = state => state.favourites.favourites
export const isFavouritedSelector = (state, id) => { return (getFavourites(state)).includes(id) }
