export const FETCH_FAVOURITES_TYPE = 'FETCH_FAVOURITES'
export const TOGGLE_FAVOURITE_TYPE = 'TOGGLE_FAVOURITE'
export const ADD_FAVOURITE_TYPE = 'ADD_FAVOURITE'
export const DEL_FAVOURITE_TYPE = 'DEL_FAVOURITE'

export const fetchFavouritesActionCreator = promise => ({
  type: FETCH_FAVOURITES_TYPE,
  payload: promise
})

export const toggleFavouriteActionCreator = entityId => ({
  type: TOGGLE_FAVOURITE_TYPE,
  payload: { entityId }
})

export const addFavouritesActionCreator = promise => ({
  type: ADD_FAVOURITE_TYPE,
  payload: promise
})

export const delFavouritesActionCreator = promise => ({
  type: DEL_FAVOURITE_TYPE,
  payload: promise
})
