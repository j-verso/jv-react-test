import React from 'react'
import injectSheet from 'react-jss'

import { RingLoader } from 'react-spinners'

const customCss = `margin:auto;margin-top:100px;`

const LoaderMask = () => {
  return <div>
    <RingLoader css={customCss} color={'#F80368'} size={450} />
  </div>
}

export default injectSheet({})(LoaderMask)
