import React from 'react'
import injectSheet from 'react-jss'

const ErrorRender = ({ message, classes }) => {
  return <div className={classes.message}>
    <p> {'Error: ' + message} </p>
  </div>
}

export default injectSheet({
  message: {
    color: '#F80368'
  }
})(ErrorRender)
